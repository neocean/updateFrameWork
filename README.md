#updateFrameWork

## 1.配置upgradesdk

将以下信息放在AndroidMannifest.xml的Application节点下，填写申请的appkey和appsecret
<meta-data android:name="com.qiyun.appkey" android:value="577ca61746f3b"></meta-data>
<meta-data android:name="com.qiyun.appsecret" android:value="d55e7ff1b7c5d9783e10c788d4523790"></meta-data>

## 2.使用upgradesdk

正常使用
CheckUpdate checkUpdate=new CheckUpdate(this);
checkUpdate.checkVersion();

回调使用
CheckUpdate checkUpdate=new CheckUpdate(this);
CheckUpdate checkUpdate=new CheckUpdate(this);
        checkUpdate.checkVersionByCallback(new CheckUpdate.UpdateCallback(){

            @Override
            public void onSuccess(VersionInfo versionInfo) {

            }

            @Override
            public void onError(VersionInfo versionInfo) {

            }
});

## 3.问题
如果用户已忽略,可使用 showUpdateDialog 强制弹出