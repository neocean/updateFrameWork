package com.qiyun.upgrade;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.LauncherApps;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.qiyun.upgrade.dialog.Builder;
import com.qiyun.upgrade.dialog.ConfirmDialogListener;
import com.qiyun.upgrade.dialog.UpdateDialog;
import com.qiyun.upgrade.helper.CheckVersionPrefrence;
import com.qiyun.upgrade.helper.NotifyHelper;
import com.qiyun.upgrade.helper.UpgradeHelper;
import com.qiyun.upgrade.model.VersionInfo;

import junit.runner.Version;

import org.xutils.common.Callback;
import org.xutils.ex.HttpException;
import org.xutils.http.RequestParams;
import org.xutils.http.request.HttpRequest;
import org.xutils.x;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * Created by Tiger on 16/7/12.
 */
public class CheckUpdate {
    private Context context;
    private String appKey;
    private String appSecret;
    private CheckVersionPrefrence prefrence;
    private VersionInfo oldVersionInfo;
    private UpdateCallback mCallback;
    private Builder dialogBuilder;
    private UpdateDialog updateDialog;
    //handler标示
    private final int PROGRESS=1001;
    private final int ERROR=1002;
    private final int SUCCESS=1003;
    public CheckUpdate(Context context){
        this.context=context;
        this.prefrence=new CheckVersionPrefrence(context);
        this.dialogBuilder= UpdateDialog.Builder(context);
        initData();
    }

    private void initData(){
        //获取appkey和appsecret
        String[] appInfo= UpgradeHelper.getAppkeyAndSecret(context);
        if(appInfo==null){
            throw new IllegalArgumentException("appkey or appsecret should not be null");
        }else{
            appKey=appInfo[0];
            appSecret=appInfo[1];
        }

    }

    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case PROGRESS:
                    NotifyHelper.getInstance(context).showNotify(msg.arg1);
                    break;
                case SUCCESS:
                    //取消通知栏
                    NotifyHelper.getInstance(context).cancelNotify();
                    String url=msg.obj.toString();
                    if(url==null) return;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(url)),
                            "application/vnd.android.package-archive");
                    context.startActivity(intent);
                    break;
                case ERROR:
                    //取消通知栏
                    NotifyHelper.getInstance(context).cancelNotify();
                    break;
            }
        }
    };


    /**
     * 检查版本
     */
    public void checkVersion(){
        checkVersionByCallback(null);
    }

    /**
     * 检查版本回调接口
     */
    public void checkVersionByCallback(final UpdateCallback callback){
        this.mCallback=callback;
        this.oldVersionInfo=this.prefrence.getVersionInfo();
        if(oldVersionInfo==null){
            getNewVersionInfo();
        }else if(oldVersionInfo.data==null){
            getNewVersionInfo();
        }else{
            int nowVersionCode=UpgradeHelper.getVersionCode(context);
            if(oldVersionInfo.data.isForceUpgrade()){
                if(nowVersionCode>=oldVersionInfo.data.getVersionCode()){
                    getNewVersionInfo();
                }else{
                    showUpdateDialog();
                }
            }else{
                getNewVersionInfo();
            }
        }

    }

    /**
     * 网络获取最新的版本信息
     */
    private void getNewVersionInfo(){
        RequestParams params=new RequestParams(Const.UPDATE_URL);
        params.addBodyParameter("appkey",appKey);
        params.addBodyParameter("appsecret",appSecret);
        params.addBodyParameter("versioncode",UpgradeHelper.getVersionCode(context)+"");
        x.http().post(params, new Callback.CommonCallback<String>() {

            @Override
            public void onSuccess(String result) {
                Log.e("result",result);
                VersionInfo versionInfo=VersionInfo.getVersionInfo(result);
                try {
                    if (versionInfo != null && versionInfo.data != null) {
                        prefrence.saveVersionInfo(result);
                        //如果是用户忽略过且不是强制更新的版本,则不提示
                        if (prefrence.getIgnoreVersion() == versionInfo.data.getVersionCode() && !versionInfo.data.isForceUpgrade()) {
                            if (mCallback != null) mCallback.onSuccess(versionInfo);
                            return;
                        }
                        showUpdateDialog();
                        if (mCallback != null) mCallback.onSuccess(versionInfo);
                    } else {
                        if (mCallback != null) mCallback.onError(versionInfo);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    if (mCallback != null) mCallback.onError(versionInfo);
                }
            }

            @Override
            public void onError(Throwable throwable, boolean b) {
                Toast.makeText(context,R.string.network_error,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancelled(CancelledException e) {

            }

            @Override
            public void onFinished() {

            }
        });
    }

    /**
     * 提示更新对话框
     */
    public void showUpdateDialog(){
        final VersionInfo versionInfo=prefrence.getVersionInfo();
        dialogBuilder.setTitle("检查到新版本");
        dialogBuilder.setContent(versionInfo.data.getRemark());
        dialogBuilder.setSubmitButton("更新", new ConfirmDialogListener() {
            @Override
            public void onClick(Dialog dialog, View view) {
                downloadAPK(versionInfo);
                dialog.hide();
            }
        });
        //强制更新
        if(versionInfo.data.isForceUpgrade()){
            updateDialog=dialogBuilder.build();
            updateDialog.hideCancleButton();
            updateDialog.setCanceledOnTouchOutside(false);
            updateDialog.show();
        }else{
            dialogBuilder.setCancleButton("忽略", new ConfirmDialogListener() {
                @Override
                public void onClick(Dialog dialog, View view) {
                    prefrence.storeIgnoreVersion(versionInfo.data.getVersionCode());
                    dialog.hide();
                }
            });
            updateDialog=dialogBuilder.build();
            updateDialog.show();
        }

    }

    /**
     * 下载apk并安装
     * @param versionInfo
     */
    private void downloadAPK(final VersionInfo versionInfo){
        new Thread() {
            @Override
            public void run() {
                FileOutputStream fos = null;
                BufferedInputStream bis = null;
                HttpURLConnection httpUrl = null;
                URL url = null;
                byte[] buf = new byte[4096];
                int size = 0;
                String fileName = null;
                try
                {
                    //建立链接
                    url = new URL(Const.MAIN_URL + versionInfo.data.getApkUrl());
                    httpUrl = (HttpURLConnection) url.openConnection();
                    //连接指定的资源
                    httpUrl.connect();
                    //获取网络输入流
                    bis = new BufferedInputStream(httpUrl.getInputStream());
                    //建立文件
                    fileName = UpgradeHelper.getAPKFilePath(context);
                    if (fileName == null) return;
                    File file = new File(fileName);
                    if (!file.exists()) {
                        if (!file.getParentFile().exists()) {
                            file.getParentFile().mkdirs();
                        }
                        file.createNewFile();
                    }
                    fos = new FileOutputStream(fileName);
                    int totalSize=0;
                    int downloadPercent=0;
                    //保存文件
                    while ((size = bis.read(buf)) != -1){
                        fos.write(buf, 0, size);

                        totalSize += size;
                        int percent=(int)(((float)totalSize/httpUrl.getContentLength())*100);
                            //发送进度
                        downloadPercent=percent;
                        Message msg = mHandler.obtainMessage(PROGRESS);
                        msg.what = PROGRESS;
                        msg.arg1 = (int) (((float) totalSize / httpUrl.getContentLength()) * 100);
                        Log.e("error", totalSize + "----" + msg.arg1 + "%");
                        mHandler.sendMessage(msg);
                    }
                    fos.close();
                    bis.close();
                    httpUrl.disconnect();

                    Message msg=mHandler.obtainMessage();
                    msg.what=SUCCESS;
                    msg.obj=fileName;
                    mHandler.sendMessageDelayed(msg,500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * 更新回调
     */
    public interface UpdateCallback{
        public void onSuccess(VersionInfo versionInfo);
        public void onError(VersionInfo versionInfo);
    }

}
