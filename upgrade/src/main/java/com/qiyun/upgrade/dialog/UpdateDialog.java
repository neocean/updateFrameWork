package com.qiyun.upgrade.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.qiyun.upgrade.R;

/**
 * Created by Tiger on 2016/3/2.
 */
public class UpdateDialog extends Dialog{
	
	private TextView title;
	private TextView content;
	private Button submit;
	private Button cancle;
	private Builder builder;
	public UpdateDialog(Context context, Builder builder) {
		super(context, R.style.confirm_dialog);
		this.setContentView(R.layout.update_dialog);
		this.builder=builder;
		
		title=(TextView) findViewById(R.id.title);
		content=(TextView)findViewById(R.id.content);
		submit=(Button)findViewById(R.id.submit);
		cancle=(Button)findViewById(R.id.cancle);
		
		build();
		
	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public static Builder Builder(Context context){
		Builder builder= new Builder(context);
		return builder;
	}
	
	public void hideSubmitButton(){
		if(submit!=null) submit.setVisibility(View.GONE);
	}
	
	public void hideCancleButton(){
		if(cancle!=null) cancle.setVisibility(View.GONE);
	}
	
	
	private void build(){
		if(builder!=null){
			if(builder.getTitle()!=null) title.setText(builder.getTitle());
			if(builder.getContent()!=null) content.setText(builder.getContent());
			if(builder.getTitle()!=null) title.setText(builder.getTitle());
			if(builder.getSubmit()!=null) submit.setText(builder.getSubmit());
			if(builder.getTitle()!=null) cancle.setText(builder.getCancle());
			
			if(builder.getSubmitListener()!=null){
				submit.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						builder.getSubmitListener().onClick(UpdateDialog.this, v);
					}
					
				});
			}else{
				submit.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						UpdateDialog.this.cancel();
					}
				});
			}
			if(builder.getCancleListener()!=null){
				cancle.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						builder.getCancleListener().onClick(UpdateDialog.this, v);
					}
					
				});
				this.hide();
			}else{
				cancle.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						UpdateDialog.this.hide();
					}
				});
			}
		}
	}
	
	
	
	
}
