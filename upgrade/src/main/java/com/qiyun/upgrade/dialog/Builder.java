package com.qiyun.upgrade.dialog;

import android.content.Context;

public class Builder{
	
	private String title;
	private String content;
	private String submit;
	private String cancle;
	private ConfirmDialogListener submitListener;
	private ConfirmDialogListener cancleListener;
	private Context mcontext;
	
	public Builder(Context context) {
		this.mcontext=context;
	}
	
	
	public ConfirmDialogListener getSubmitListener() {
		return submitListener;
	}



	public void setSubmitListener(ConfirmDialogListener submitListener) {
		this.submitListener = submitListener;
	}



	public void ConfirmDialogListener(ConfirmDialogListener submitListener) {
		this.submitListener = submitListener;
	}

	public ConfirmDialogListener getCancleListener() {
		return cancleListener;
	}

	public void setCancleListener(ConfirmDialogListener cancleListener) {
		this.cancleListener = cancleListener;
	}

	public String getTitle() {
		return title;
	}
	public Builder setTitle(String title) {
		this.title = title;
		return this;
	}
	public String getContent() {
		return content;
	}
	public Builder setContent(String content) {
		this.content = content;
		return this;
	}
	public String getSubmit() {
		return submit;
	}
	public Builder setSubmitButton(String submit,ConfirmDialogListener lisenter) {
		this.submit = submit;
		this.submitListener=lisenter;
		return this;
	}
	public String getCancle() {
		return cancle;
	}
	public Builder setCancleButton(String cancle,ConfirmDialogListener lisenter) {
		this.cancle = cancle;
		this.cancleListener=lisenter;
		return this;
	}
	
	public UpdateDialog build(){
		return new UpdateDialog(mcontext, this);
	}
	
	
}
