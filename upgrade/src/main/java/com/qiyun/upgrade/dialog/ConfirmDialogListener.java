package com.qiyun.upgrade.dialog;

import android.app.Dialog;
import android.view.View;
/**
 * Created by Tiger on 2016/3/2.
 */
public interface ConfirmDialogListener {
	void onClick(Dialog dialog, View view);
}
