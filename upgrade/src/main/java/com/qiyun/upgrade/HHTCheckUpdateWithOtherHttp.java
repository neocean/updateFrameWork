package com.qiyun.upgrade;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.qiyun.upgrade.dialog.Builder;
import com.qiyun.upgrade.dialog.ConfirmDialogListener;
import com.qiyun.upgrade.dialog.UpdateDialog;
import com.qiyun.upgrade.helper.CheckVersionPrefrence;
import com.qiyun.upgrade.helper.HttpHelper;
import com.qiyun.upgrade.helper.NotifyHelper;
import com.qiyun.upgrade.helper.UpgradeHelper;
import com.qiyun.upgrade.model.VersionInfo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * 户户通项目使用的检查更新，先下载再提示安装
 * 不使用xutils的版本检查
 * Created by Tiger on 16/7/12.
 */
public class HHTCheckUpdateWithOtherHttp {
    private Context context;
    private String appKey;
    private String appSecret;
    private CheckVersionPrefrence prefrence;
    private Builder dialogBuilder;
    private UpdateDialog updateDialog;
    //handler标示
    private final int PROGRESS=1001;
    private final int ERROR=1002;
    private final int SUCCESS=1003;

    public SimpleDateFormat mFormat = new SimpleDateFormat("yyyy_MM_dd");

    public HHTCheckUpdateWithOtherHttp(Context context){
        this.context=context;
        this.prefrence=new CheckVersionPrefrence(context);
        this.dialogBuilder= UpdateDialog.Builder(context);
        initData();
    }
    private void initData(){
        //获取appkey和appsecret
        String[] appInfo= UpgradeHelper.getAppkeyAndSecret(context);
        if(appInfo==null){
            throw new IllegalArgumentException("appkey or appsecret should not be null");
        }else{
            appKey=appInfo[0];
            appSecret=appInfo[1];
        }

    }

    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case PROGRESS:
                    NotifyHelper.getInstance(context).showNotify(msg.arg1);
                    break;
                case SUCCESS:
                    //取消通知栏
                    NotifyHelper.getInstance(context).cancelNotify();
                    String hint=msg.obj.toString();
                    showNewVersionDialog(msg.arg1,hint);

                    break;
                case ERROR:
                    //取消通知栏
                    NotifyHelper.getInstance(context).cancelNotify();
                    break;
            }
        }
    };


    /**
     * 户户通app检查版本,但没有忽略版本选项
     */
    public void HHTCheckVersion(){

        //今天未检查
        if(!todayAlreadyChecked()) {

            //没有已下载的新版本，则网络请求新信息
            if(!hasDownloadedNewVersion()) {

                HttpHelper.getsInstance().checkVersionInfo(getParam(), new HttpHelper.HttpResultCallback() {
                    @Override
                    public void onSuccess(VersionInfo versionInfo) {
                        try {
                            if (versionInfo != null) {

                                if(versionInfo.data != null) {

                                    int now = UpgradeHelper.getVersionCode(context);
                                    //有新版本
                                    if(versionInfo.data.getVersionCode() > now) {

                                        //不是强制更新
                                        if(!versionInfo.data.isForceUpgrade()) {

                                            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                                            NetworkInfo info = manager.getActiveNetworkInfo();
                                            if (info != null) {
                                                if (NetworkInfo.State.CONNECTED == info.getState() && info.isAvailable()) {

                                                    if (info.getType() == ConnectivityManager.TYPE_WIFI) {

                                                        downloadAPK(versionInfo);
                                                    } else if (info.getType() == ConnectivityManager.TYPE_MOBILE) {

                                                        finishCheck();
                                                    }
                                                    //无网络
                                                } else {

                                                    finishCheck();
                                                }

                                                //无网络
                                            } else {

                                                finishCheck();
                                            }

                                        }else {

                                            downloadAPK(versionInfo);
                                        }

                                    }else {

                                        finishCheck();
                                    }

                                }else {
                                    //     Toast.makeText(context,versionInfo.message,Toast.LENGTH_SHORT).show();

                                    finishCheck();
                                }

                            } else {
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFalse(String e) {

                        finishCheck();
                    }
                });

                //有已下载的新版本
            }else {

                showNewVersionDialog(prefrence.getDownloadedVersion(),"是否安装新版本");
            }
        }else {

            finishCheck();
        }
    }
    private void showNewVersionDialog(final int versionCode, String hint) {


        dialogBuilder.setTitle("检查到新版本");
        dialogBuilder.setContent(hint);
        dialogBuilder.setSubmitButton("更新", new ConfirmDialogListener() {
            @Override
            public void onClick(Dialog dialog, View view) {
                dialog.hide();

                //删除旧版本的apk
                int now = UpgradeHelper.getVersionCode(context);
                String nowFileName = UpgradeHelper.getAPKFilePath(context,now);
                File nowFile = new File(nowFileName);
                if(nowFile.exists()) {
                    nowFile.delete();
                }

                String fileName = UpgradeHelper.getAPKFilePath(context,versionCode);
                File file = new File(fileName);
                if(!file.exists()) {
                    prefrence.setDownloadedVersion(0);
                    finishCheck();
                    return;
                }
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(new File(fileName)),
                        "application/vnd.android.package-archive");

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                try{
                    if(intent.resolveActivity(context.getPackageManager()) != null) {
                        context.startActivity(intent);


                    }else {

                        Toast.makeText(context,"未找到安装应用",Toast.LENGTH_SHORT).show();
                    }
                }catch (Exception e) {
                    Toast.makeText(context,"安装出现错误，" + e.getCause().toString(),Toast.LENGTH_LONG).show();
                }


            }
        });
        dialogBuilder.setCancleButton("取消", new ConfirmDialogListener() {
            @Override
            public void onClick(Dialog dialog, View view) {

                dialog.hide();
            }
        });

        if(updateDialog != null) {
            updateDialog.dismiss();
        }

        updateDialog=dialogBuilder.build();
        updateDialog.show();

        finishCheck();
    }


    /**
     * 是否有未安装的已下载的新版本
     * @return
     */
    private boolean hasDownloadedNewVersion() {
        int now = UpgradeHelper.getVersionCode(context);
        int downloaded = prefrence.getDownloadedVersion();
        if(now >= downloaded) {

            return false;
        }else {
            return true;
        }

    }


    /**
     * 今天是否检查过
     * @return
     */
    private boolean todayAlreadyChecked() {
        String today = mFormat.format(new Date());
        String lastCheckDate = prefrence.getLastCheckTime();

        if(TextUtils.equals(today,lastCheckDate)) {
            return true;
        }else {
            return false;
        }
    }


    private Map<String,String> getParam() {
        Map<String,String> params = new HashMap<String,String>();
        params.put("appkey",appKey);
        params.put("appsecret",appSecret);
        params.put("versioncode",UpgradeHelper.getVersionCode(context)+"");
        return params;

    }


    /**
     * 下载apk并安装
     * @param versionInfo
     */
    private void downloadAPK(final VersionInfo versionInfo){
        new Thread() {
            @Override
            public void run() {
                FileOutputStream fos = null;
                BufferedInputStream bis = null;
                HttpURLConnection httpUrl = null;
                URL url = null;
                //原先是4096，改小了；要不然报解析包出现问题。
                byte[] buf = new byte[64];
                int size = 0;
                String fileName = null;
                try
                {
                    //建立链接
                    url = new URL(Const.MAIN_URL + versionInfo.data.getApkUrl());
                    httpUrl = (HttpURLConnection) url.openConnection();
                    //连接指定的资源
                    httpUrl.connect();
                    //获取网络输入流
                    bis = new BufferedInputStream(httpUrl.getInputStream());
                    //建立文件
                    fileName = UpgradeHelper.getAPKFilePath(context,versionInfo.data.getVersionCode());
                    if (fileName == null) return;
                    File file = new File(fileName);
                    if (!file.exists()) {
                        if (!file.getParentFile().exists()) {
                            file.getParentFile().mkdirs();
                        }
                        file.createNewFile();
                    }
                    fos = new FileOutputStream(fileName);
                    int totalSize=0;
                    int downloadPercent=0;
                    //保存文件
                    while ((size = bis.read(buf)) != -1){
                        fos.write(buf, 0, size);

                        totalSize += size;
                        int percent=(int)(((float)totalSize/httpUrl.getContentLength())*100);
                        if(percent-downloadPercent>=2) {
                            //发送进度
                            downloadPercent=percent;
                            Message msg = mHandler.obtainMessage(PROGRESS);
                            msg.what = PROGRESS;
                            msg.arg1 = (int) (((float) totalSize / httpUrl.getContentLength()) * 100);
                            Log.e("error", totalSize + "----" + msg.arg1 + "%");
                            mHandler.sendMessage(msg);
                        }
                    }
                    fos.close();
                    bis.close();
                    httpUrl.disconnect();

                    //进行文件完整性校验
                    String md5 = UpgradeHelper.getFileMD5(file);
                    if(TextUtils.equals(md5,versionInfo.data.getMd5())) {
                        Message msg=mHandler.obtainMessage();
                        msg.what=SUCCESS;
                        String hint = versionInfo.data.getRemark();
                        if(hint == null) {
                            hint = "发现新版本";
                        }
                        msg.obj= hint;
                        msg.arg1 = versionInfo.data.getVersionCode();
                        mHandler.sendMessageDelayed(msg,500);

                        //更新已下载的文件
                        prefrence.setDownloadedVersion(versionInfo.data.getVersionCode());
                        Log.e("http","md5校验通过");

                    }else {

                        prefrence.setDownloadedVersion(0);
                        if(file.exists()) {
                            file.delete();

                        }
                        Log.e("http","md5校验失败:" + versionInfo.data.getMd5() + "|" + md5);

                        finishCheck();


                    }



                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void finishCheck() {

        prefrence.setLastCheckTime(mFormat.format(new Date()));
    }

}
