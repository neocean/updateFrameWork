package com.qiyun.upgrade.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Tiger on 16/7/18.
 */
public class VersionInfo implements Serializable{
    public int status;
    public String message;
    public VersionInfo.Content data;

    public static class Content implements Serializable {
        @SerializedName("versioncode")
        private int versionCode;
        @SerializedName("versiontag")
        private String versionTag;
        @SerializedName("isforce")
        private boolean isForceUpgrade;
        @SerializedName("apkurl")
        private String apkUrl;
        private String remark;

        private String md5;

        public String getMd5() {
            return md5;
        }

        public void setMd5(String md5) {
            this.md5 = md5;
        }

        public int getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(int versionCode) {
            this.versionCode = versionCode;
        }

        public String getVersionTag() {
            return versionTag;
        }

        public void setVersionTag(String versionTag) {
            this.versionTag = versionTag;
        }

        public boolean isForceUpgrade() {
            return isForceUpgrade;
        }

        public void setForceUpgrade(boolean forceUpgrade) {
            isForceUpgrade = forceUpgrade;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getApkUrl() {
            return apkUrl;
        }

        public void setApkUrl(String apkUrl) {
            this.apkUrl = apkUrl;
        }
    }
    /**
     * json获取versioninfo实例
     * @param json
     * @return
     */
    public static VersionInfo getVersionInfo(String json){
        Gson gson=new Gson();
        try {
            VersionInfo versionInfo = gson.fromJson(json, VersionInfo.class);
            return versionInfo;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
