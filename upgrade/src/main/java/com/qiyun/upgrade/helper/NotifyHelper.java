package com.qiyun.upgrade.helper;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Parcel;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.qiyun.upgrade.R;

/**
 * Created by mac on 16/7/22.
 */
public class NotifyHelper {
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    private Notification mNotification;
    private static NotifyHelper notifyHelper;
    private final int NOTIFYID=10011;
    private Context context;
    private RemoteViews rv;
    public static NotifyHelper getInstance(Context context){
        if(notifyHelper==null){
            notifyHelper=new NotifyHelper(context);
        }
        return notifyHelper;
    }

    private NotifyHelper(Context context){
        this.context=context;
        mBuilder = new NotificationCompat.Builder(context);
        mBuilder.setSmallIcon(R.drawable.icon_upgrade);
        mBuilder.setContentTitle("下载");
        mBuilder.setContentText("正在下载");
        mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(NOTIFYID, mBuilder.build());
        mBuilder.setProgress(100,0,false);
    }

    public void showNotify(int progress){
        mBuilder.setProgress(100,progress,false);
        mNotificationManager.notify(NOTIFYID,mBuilder.build());
        //下载进度提示
        mBuilder.setContentText("下载"+progress+"%");
    }

    public void cancelNotify(){
        mNotificationManager.cancel(NOTIFYID);
    }

}
