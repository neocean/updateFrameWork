package com.qiyun.upgrade.helper;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.google.gson.Gson;
import com.qiyun.upgrade.Const;
import com.qiyun.upgrade.model.VersionInfo;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by chandlerbing on 2017/3/27.
 */

public class HttpHelper {

    private static HttpHelper sInstance = new HttpHelper();
    private HttpResultCallback mResultCallback;
    private static final int SUCCESS = 101;
    private static final int FALSE = 102;

    private Handler mMainHandler;
    public static HttpHelper getsInstance() {
        return sInstance;
    }

    private HttpHelper() {
        super();
        mMainHandler = new Handler(Looper.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {

                switch (msg.what) {

                    case SUCCESS:
                        if(mResultCallback != null) {
                            VersionInfo info = (VersionInfo) msg.obj;
                            mResultCallback.onSuccess(info);
                        }

                        break;
                    case FALSE:
                        if(mResultCallback != null) {
                            String error = (String) msg.obj;
                            mResultCallback.onFalse(error);
                        }

                        break;
                }

            }
        };

    }





    public void checkVersionInfo(final Map<String,String> params,HttpResultCallback callback){
        this.mResultCallback = callback;

        new Thread() {
            @Override
            public void run() {
                FileOutputStream fos = null;
                HttpURLConnection httpUrl = null;
                URL url = null;
                byte[] buf = new byte[4096];
                int size = 0;
                String fileName = null;
                try
                {
                    //建立链接
                    url = new URL(Const.UPDATE_URL);
                    httpUrl = (HttpURLConnection) url.openConnection();
                    httpUrl.setReadTimeout(5000);
                    httpUrl.setConnectTimeout(5000);
                    httpUrl.setRequestMethod("POST");
                    httpUrl.setDoOutput(true);
                    httpUrl.setDoInput(true);
                    httpUrl.setRequestProperty("Content-Type"
                            ,"application/x-www-form-urlencoded; charset=UTF-8");
                    DataOutputStream out = new DataOutputStream(httpUrl.getOutputStream());
                    out.write(getBody(params));
                    out.close();

                    //连接指定的资源
                    httpUrl.connect();
                    //获取网络输入流
                    //建立文件


                    InputStream is = httpUrl.getInputStream();
                    int length = 0;
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    while((length = is.read(buf)) != -1) {
                        bos.write(buf,0,length);

                    }

                    byte[] bytes = bos.toByteArray();

                    String result = new String(bytes,"UTF-8");

                    is.close();
                    bos.close();
                    httpUrl.disconnect();

                    Log.e("http",result);

                    Message msg=mMainHandler.obtainMessage();
                    msg.what=SUCCESS;
                    VersionInfo info = new Gson().fromJson(result,VersionInfo.class);
                    msg.obj=info;
                    mMainHandler.sendMessage(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                    Message msg=mMainHandler.obtainMessage();
                    msg.what=FALSE;
                    msg.obj=e.toString();
                    mMainHandler.sendMessage(msg);

                }
            }
        }.start();
    }

    private byte[] getBody(Map<String,String> params) {
        if(params != null && params.size() > 0) {
            return encodeParameters(params,"UTF-8");
        }
        return null;
    }





    private byte[] encodeParameters(Map<String,String> params,String paramsEncoding){
        StringBuilder encodedParams = new StringBuilder();

        try {
            for(Map.Entry<String,String> entry : params.entrySet()) {
                encodedParams.append(URLEncoder.encode(entry.getKey(),paramsEncoding));
                //为什么是单引号
                encodedParams.append('=');
                encodedParams.append(URLEncoder.encode(entry.getValue(),paramsEncoding));
                encodedParams.append('&');
            }
            return encodedParams.toString().getBytes(paramsEncoding);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Encoding not supported:" + paramsEncoding,e);
        }


    }



    public interface HttpResultCallback{

        public void onSuccess(VersionInfo info);
        public void onFalse(String e);

    }

}
