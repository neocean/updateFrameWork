package com.qiyun.upgrade.helper;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Tiger on 16/7/12.
 */
public class UpgradeHelper {
    public static final String SDCARD_PATH= Environment.getExternalStorageDirectory().getAbsolutePath();
    /**
     *获取本应用的key和密钥
     * @param context
     * @return String[] 0 appkey 1 appsecret
     */
    public static String[] getAppkeyAndSecret(Context context){
        ApplicationInfo info= null;
        try {
            info = context.getPackageManager()
                    .getApplicationInfo(context.getPackageName(),
                            PackageManager.GET_META_DATA);
            String appkey =info.metaData.getString("com.qiyun.appkey");
            String appsecret =info.metaData.getString("com.qiyun.appsecret");
            if(!TextUtils.isEmpty(appkey) && !TextUtils.isEmpty(appsecret)){
                String[] appInfo=new String[2];
                appInfo[0]=appkey;
                appInfo[1]=appsecret;
                return appInfo;
            }else{
                return null;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取本应用的versionCode
     */

    public static int getVersionCode(Context context){
        try {
            PackageInfo pi=context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pi.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 获取apk保存的路径  包名 +版本号
     */
    public static String getAPKFilePath(Context context){
        if(SDCARD_PATH!=null){
            File file=new File(SDCARD_PATH+"/"+context.getPackageName() +".apk");
            if(file.exists()){
                file.delete();
            }
            return file.getAbsolutePath();
        }
        return null;
    }

    /**
     * 获取apk保存的路径  包名 +版本号
     */
    public static String getAPKFilePath(Context context,int versionCode){


        if(SDCARD_PATH!=null){
            File file=new File(SDCARD_PATH+"/"+context.getPackageName() + versionCode +".apk");

            return file.getAbsolutePath();
        }
        return null;
    }

    /**
     * get file md5
     * @param file
     * @return
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    public static String getFileMD5(File file) throws NoSuchAlgorithmException, IOException {
        if (!file.isFile()) {
            return null;
        }
        MessageDigest digest;
        FileInputStream in;
        byte buffer[] = new byte[1024];
        int len;
        digest = MessageDigest.getInstance("MD5");
        in = new FileInputStream(file);
        while ((len = in.read(buffer, 0, 1024)) != -1) {
            digest.update(buffer, 0, len);
        }
        in.close();
        BigInteger bigInt = new BigInteger(1, digest.digest());
        return bigInt.toString(16);
    }
}
