package com.qiyun.upgrade.helper;

import android.content.Context;
import android.content.SharedPreferences;

import com.qiyun.upgrade.model.VersionInfo;

/**
 * Created by Tiger on 16/7/18.
 */
public class CheckVersionPrefrence {
    private Context context;
    private SharedPreferences sp;
    private String packName="com.qiyun.upgrade";

    private final String KEY_DOWNLOADED_VERSION = "downloadedversion";
    private final String KEY_LAST_CHECK_TIME = "lastchecktime";

    public CheckVersionPrefrence(Context context){
        this.context=context;
        this.packName=context.getPackageName();
        this.sp=context.getSharedPreferences(this.packName,0);
    }

    /**
     * 保存版本信息
     * @param data
     */
    public void saveVersionInfo(String data){
        SharedPreferences.Editor editor=this.sp.edit();
        editor.putString("versionInfo",data);
        editor.commit();
    }

    /**
     * 获取VersionInfo
     * @return
     */
    public VersionInfo getVersionInfo(){
        String result=this.sp.getString("versionInfo",null);
        VersionInfo mVersionInfo=null;
        if(result==null){
            return mVersionInfo;
        }else if(result.length()==0){
            return mVersionInfo;
        }else{
            mVersionInfo=VersionInfo.getVersionInfo(result);
            return mVersionInfo;
        }
    }

    /**
     * 存储忽略版本号
     * @param versionCode
     */
    public void storeIgnoreVersion(int versionCode){
        SharedPreferences.Editor editor=this.sp.edit();
        editor.putInt("IgnoreVersionCode",versionCode);
        editor.commit();
    }

    public int getIgnoreVersion(){
        return this.sp.getInt("IgnoreVersionCode",0);
    }


    public int getDownloadedVersion() {
        return sp.getInt(KEY_DOWNLOADED_VERSION,0);
    }

    public void setDownloadedVersion(int version) {
        sp.edit().putInt(KEY_DOWNLOADED_VERSION,version).commit();
    }

    public String getLastCheckTime() {
        return sp.getString(KEY_LAST_CHECK_TIME,null);
    }

    public void setLastCheckTime(String time) {

        sp.edit().putString(KEY_LAST_CHECK_TIME,time).commit();
    }
}
