package com.qiyun.upgrade;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.qiyun.upgrade.dialog.Builder;
import com.qiyun.upgrade.dialog.ConfirmDialogListener;
import com.qiyun.upgrade.dialog.UpdateDialog;
import com.qiyun.upgrade.helper.CheckVersionPrefrence;
import com.qiyun.upgrade.helper.HttpHelper;
import com.qiyun.upgrade.helper.NotifyHelper;
import com.qiyun.upgrade.helper.UpgradeHelper;
import com.qiyun.upgrade.model.VersionInfo;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * 不使用xutils的版本检查
 * Created by Tiger on 16/7/12.
 */
public class CheckUpdateWithOtherHttp {
    private Context context;
    private String appKey;
    private String appSecret;
    private CheckVersionPrefrence prefrence;
    private VersionInfo oldVersionInfo;
    private UpdateCallback mCallback;
    private Builder dialogBuilder;
    private UpdateDialog updateDialog;
    //handler标示
    private final int PROGRESS=1001;
    private final int ERROR=1002;
    private final int SUCCESS=1003;
    public CheckUpdateWithOtherHttp(Context context){
       this(context,null);
    }


    public CheckUpdateWithOtherHttp(Context context,UpdateCallback callback){
        this.context=context;
        this.prefrence=new CheckVersionPrefrence(context);
        this.dialogBuilder= UpdateDialog.Builder(context);
        initData();
        this.mCallback = callback;
    }
    private void initData(){
        //获取appkey和appsecret
        String[] appInfo= UpgradeHelper.getAppkeyAndSecret(context);
        if(appInfo==null){
            throw new IllegalArgumentException("appkey or appsecret should not be null");
        }else{
            appKey=appInfo[0];
            appSecret=appInfo[1];
        }

    }

    private Handler mHandler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case PROGRESS:
                    NotifyHelper.getInstance(context).showNotify(msg.arg1);
                    break;
                case SUCCESS:
                    //取消通知栏
                    NotifyHelper.getInstance(context).cancelNotify();
                    String url=msg.obj.toString();
                    if(url==null) return;
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File(url)),
                            "application/vnd.android.package-archive");

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    try{
                        if(intent.resolveActivity(context.getPackageManager()) != null) {
                            context.startActivity(intent);

                            if(mCallback != null) {
                                mCallback.onStartInstallActivity();
                            }

                        }else {

                            Toast.makeText(context,"未找到安装应用",Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e) {
                        Toast.makeText(context,"安装出现错误，" + e.getCause().toString(),Toast.LENGTH_LONG).show();
                    }

                    break;
                case ERROR:
                    //取消通知栏
                    NotifyHelper.getInstance(context).cancelNotify();
                    break;
            }
        }
    };


    /**
     * 检查版本
     */
    public void checkVersionInHomeActivity(){
        checkVersionByCallbackInHomeActivity(null);
    }

    /**
     * 检查版本,但没有忽略版本选项
     */
    public void checkVersionInHomeActivityWithoutIgnore(){
        checkVersionByCallbackInHomeActivityWithoutIgnore(null);
    }

    public void checkVersionInSettingActivity(){
        checkVersionByCallbackInSettingActivity(null);
    }
    /**
     * 检查版本回调接口
     */
    public void checkVersionByCallbackInHomeActivity(final UpdateCallback callback){
        this.mCallback=callback;
        this.oldVersionInfo=this.prefrence.getVersionInfo();
        if(oldVersionInfo==null){
            getNewVersionInfoInHomeActivity();
        }else if(oldVersionInfo.data==null){
            getNewVersionInfoInHomeActivity();
        }else{
            int nowVersionCode=UpgradeHelper.getVersionCode(context);
            if(oldVersionInfo.data.isForceUpgrade()){
                if(nowVersionCode>=oldVersionInfo.data.getVersionCode()){
                    getNewVersionInfoInHomeActivity();
                }else{
                    showUpdateDialog();
                }
            }else{
                getNewVersionInfoInHomeActivity();
            }
        }

    }

    /**
     * 检查版本回调接口,但没有忽略版本选项
     */
    public void checkVersionByCallbackInHomeActivityWithoutIgnore(final UpdateCallback callback){
        this.mCallback=callback;
        this.oldVersionInfo=this.prefrence.getVersionInfo();
        if(oldVersionInfo==null){
            getNewVersionInfoInHomeActivityWithoutIgnore();
        }else if(oldVersionInfo.data==null){
            getNewVersionInfoInHomeActivityWithoutIgnore();
        }else{
            int nowVersionCode=UpgradeHelper.getVersionCode(context);
            if(oldVersionInfo.data.isForceUpgrade()){
                if(nowVersionCode>=oldVersionInfo.data.getVersionCode()){
                    getNewVersionInfoInHomeActivityWithoutIgnore();
                }else{
                    showUpdateDialogWithoutIgnore();
                }
            }else{
                getNewVersionInfoInHomeActivityWithoutIgnore();
            }
        }

    }

    public void checkVersionByCallbackInSettingActivity(final UpdateCallback callback){
        this.mCallback=callback;
        this.oldVersionInfo=this.prefrence.getVersionInfo();
        if(oldVersionInfo==null){
            getNewVersionInfoInSettingActivity();
        }else if(oldVersionInfo.data==null){
            getNewVersionInfoInSettingActivity();
        }else{
            int nowVersionCode=UpgradeHelper.getVersionCode(context);
            if(oldVersionInfo.data.isForceUpgrade()){
                if(nowVersionCode>=oldVersionInfo.data.getVersionCode()){
                    getNewVersionInfoInSettingActivity();
                }else{
                    showUpdateDialog();
                }
            }else{
                getNewVersionInfoInSettingActivity();
            }
        }

    }

    private Map<String,String> getParam() {
        Map<String,String> params = new HashMap<String,String>();
        params.put("appkey",appKey);
        params.put("appsecret",appSecret);
        params.put("versioncode",UpgradeHelper.getVersionCode(context)+"");
        return params;

    }

    /**
     * 网络获取最新的版本信息
     */
    private void getNewVersionInfoInHomeActivity(){

        HttpHelper.getsInstance().checkVersionInfo(getParam(), new HttpHelper.HttpResultCallback() {
            @Override
            public void onSuccess(VersionInfo versionInfo) {
                try {
                    if (versionInfo != null) {

                        if(versionInfo.data != null) {
                            String result = new Gson().toJson(versionInfo);
                            prefrence.saveVersionInfo(result);
                            //如果是用户忽略过且不是强制更新的版本,则不提示
                            if (prefrence.getIgnoreVersion() == versionInfo.data.getVersionCode() && !versionInfo.data.isForceUpgrade()) {
                                if (mCallback != null) mCallback.onSuccess(versionInfo);
                                return;
                            }
                            showUpdateDialog();
                            if (mCallback != null) mCallback.onSuccess(versionInfo);
                        }else {
                       //     Toast.makeText(context,versionInfo.message,Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (mCallback != null) mCallback.onError(versionInfo);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    if (mCallback != null) mCallback.onError(versionInfo);
                }

            }

            @Override
            public void onFalse(String e) {

                Toast.makeText(context,R.string.network_error,Toast.LENGTH_LONG).show();

            }
        });
    }

    /**
     * 网络获取最新的版本信息
     */
    private void getNewVersionInfoInHomeActivityWithoutIgnore(){

        HttpHelper.getsInstance().checkVersionInfo(getParam(), new HttpHelper.HttpResultCallback() {
            @Override
            public void onSuccess(VersionInfo versionInfo) {
                try {
                    if (versionInfo != null) {

                        if(versionInfo.data != null) {
                            String result = new Gson().toJson(versionInfo);
                            prefrence.saveVersionInfo(result);
                            //如果是用户忽略过且不是强制更新的版本,则不提示
                            if (prefrence.getIgnoreVersion() == versionInfo.data.getVersionCode() && !versionInfo.data.isForceUpgrade()) {
                                if (mCallback != null) mCallback.onSuccess(versionInfo);
                                return;
                            }
                            showUpdateDialogWithoutIgnore();
                            if (mCallback != null) mCallback.onSuccess(versionInfo);
                        }else {
                            //     Toast.makeText(context,versionInfo.message,Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (mCallback != null) mCallback.onError(versionInfo);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    if (mCallback != null) mCallback.onError(versionInfo);
                }

            }

            @Override
            public void onFalse(String e) {

        //        Toast.makeText(context,R.string.network_error,Toast.LENGTH_LONG).show();

            }
        });
    }


    private void getNewVersionInfoInSettingActivity(){

        HttpHelper.getsInstance().checkVersionInfo(getParam(), new HttpHelper.HttpResultCallback() {
            @Override
            public void onSuccess(VersionInfo versionInfo) {
                try {
                    if (versionInfo != null) {

                        if(versionInfo.data != null) {


                            showUpdateDialog();
                            if (mCallback != null) mCallback.onSuccess(versionInfo);
                        }else {
                            Toast.makeText(context,versionInfo.message,Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        if (mCallback != null) mCallback.onError(versionInfo);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    if (mCallback != null) mCallback.onError(versionInfo);
                }

            }

            @Override
            public void onFalse(String e) {

                Toast.makeText(context,R.string.network_error,Toast.LENGTH_LONG).show();

            }
        });
    }


    /**
     * 提示更新对话框
     */
    public void showUpdateDialog(){
        final VersionInfo versionInfo=prefrence.getVersionInfo();
        dialogBuilder.setTitle("检查到新版本");
        dialogBuilder.setContent(versionInfo.data.getRemark());
        dialogBuilder.setSubmitButton("更新", new ConfirmDialogListener() {
            @Override
            public void onClick(Dialog dialog, View view) {
                downloadAPK(versionInfo);
                dialog.hide();
            }
        });
        //强制更新
        if(versionInfo.data.isForceUpgrade()){
            updateDialog=dialogBuilder.build();
            updateDialog.hideCancleButton();
            updateDialog.setCanceledOnTouchOutside(false);
            updateDialog.show();
        }else{
            dialogBuilder.setCancleButton("忽略", new ConfirmDialogListener() {
                @Override
                public void onClick(Dialog dialog, View view) {
                    prefrence.storeIgnoreVersion(versionInfo.data.getVersionCode());
                    dialog.hide();
                }
            });
            updateDialog=dialogBuilder.build();
            updateDialog.show();
        }

    }

    /**
     * 提示更新对话框,但没有忽略版本选型
     */
    public void showUpdateDialogWithoutIgnore(){
        final VersionInfo versionInfo=prefrence.getVersionInfo();
        if(updateDialog != null) {
            updateDialog.dismiss();
            updateDialog = null;
        }
        dialogBuilder.setTitle("检查到新版本");
        dialogBuilder.setContent(versionInfo.data.getRemark());
        dialogBuilder.setSubmitButton("更新", new ConfirmDialogListener() {
            @Override
            public void onClick(Dialog dialog, View view) {
                downloadAPK(versionInfo);
                dialog.hide();
                updateDialog = null;

            }
        });
        //强制更新
        if(versionInfo.data.isForceUpgrade()){
            updateDialog=dialogBuilder.build();
            updateDialog.hideCancleButton();
            updateDialog.setCanceledOnTouchOutside(false);
            updateDialog.show();
        }else{
//            dialogBuilder.setCancleButton("忽略", new ConfirmDialogListener() {
//                @Override
//                public void onClick(Dialog dialog, View view) {
//                    prefrence.storeIgnoreVersion(versionInfo.data.getVersionCode());
//                    dialog.hide();
//                }
//            });

            dialogBuilder.setCancleButton("取消", new ConfirmDialogListener() {
                @Override
                public void onClick(Dialog dialog, View view) {
                    dialog.hide();
                    updateDialog = null;
                }
            });
            updateDialog=dialogBuilder.build();
            updateDialog.show();
        }

    }

    /**
     * 下载apk并安装
     * @param versionInfo
     */
    private void downloadAPK(final VersionInfo versionInfo){
        new Thread() {
            @Override
            public void run() {
                FileOutputStream fos = null;
                BufferedInputStream bis = null;
                HttpURLConnection httpUrl = null;
                URL url = null;
                //原先是4096，改小了；要不然报解析包出现问题。
                byte[] buf = new byte[64];
                int size = 0;
                String fileName = null;
                try
                {
                    //建立链接
                    url = new URL(Const.MAIN_URL + versionInfo.data.getApkUrl());
                    httpUrl = (HttpURLConnection) url.openConnection();
                    //连接指定的资源
                    httpUrl.connect();
                    //获取网络输入流
                    bis = new BufferedInputStream(httpUrl.getInputStream());
                    //建立文件
                    fileName = UpgradeHelper.getAPKFilePath(context);
                    if (fileName == null) return;
                    File file = new File(fileName);
                    if (!file.exists()) {
                        if (!file.getParentFile().exists()) {
                            file.getParentFile().mkdirs();
                        }
                        file.createNewFile();
                    }
                    fos = new FileOutputStream(fileName);
                    int totalSize=0;
                    int downloadPercent=0;
                    //保存文件
                    while ((size = bis.read(buf)) != -1){
                        fos.write(buf, 0, size);

                        totalSize += size;
                        int percent=(int)(((float)totalSize/httpUrl.getContentLength())*100);
                        if(percent-downloadPercent>=2) {
                            //发送进度
                            downloadPercent=percent;
                            Message msg = mHandler.obtainMessage(PROGRESS);
                            msg.what = PROGRESS;
                            msg.arg1 = (int) (((float) totalSize / httpUrl.getContentLength()) * 100);
                            Log.e("error", totalSize + "----" + msg.arg1 + "%");
                            mHandler.sendMessage(msg);
                        }
                    }
                    fos.close();
                    bis.close();
                    httpUrl.disconnect();

                    Message msg=mHandler.obtainMessage();
                    msg.what=SUCCESS;
                    msg.obj=fileName;
                    mHandler.sendMessageDelayed(msg,500);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * 更新回调
     */
    public interface UpdateCallback{
        public void onSuccess(VersionInfo versionInfo);
        public void onError(VersionInfo versionInfo);

        public void onStartInstallActivity();
    }

}
