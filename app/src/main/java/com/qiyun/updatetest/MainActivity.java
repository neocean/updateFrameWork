package com.qiyun.updatetest;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import com.qiyun.upgrade.CheckUpdate;
import com.qiyun.upgrade.CheckUpdateWithOtherHttp;
import com.qiyun.upgrade.HHTCheckUpdateWithOtherHttp;
import com.qiyun.upgrade.helper.UpgradeHelper;
import com.qiyun.upgrade.model.VersionInfo;

import org.xutils.x;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        CheckUpdate checkUpdate=new CheckUpdate(this);
//        checkUpdate.checkVersionByCallbackInHomeActivity(new CheckUpdate.UpdateCallback(){
//
//            @Override
//            public void onSuccess(VersionInfo versionInfo) {
//
//            }
//
//            @Override
//            public void onError(VersionInfo versionInfo) {
//
//            }
//        });
    }

    public void onUpdateInHome(View v) {
        CheckUpdateWithOtherHttp check = new CheckUpdateWithOtherHttp(this);
        check.checkVersionInHomeActivity();
    }

    public void onUpdateInSetting(View v) {
        CheckUpdateWithOtherHttp check = new CheckUpdateWithOtherHttp(this);
        check.checkVersionInSettingActivity();
    }

    public void onUpdateHHT(View v) {
        HHTCheckUpdateWithOtherHttp check = new HHTCheckUpdateWithOtherHttp(this);
        check.HHTCheckVersion();
    }
}
