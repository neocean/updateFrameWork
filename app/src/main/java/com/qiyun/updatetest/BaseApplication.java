package com.qiyun.updatetest;

import android.app.Application;

import org.xutils.x;

/**
 * Created by mac on 2016/11/2.
 */

public class BaseApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);
    }
}
